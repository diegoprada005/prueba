import 'package:flutter/material.dart';

class IconName {
  final String name;
  final Image img;

  IconName({required this.name, required this.img});

  static final data = <IconName>[
    IconName(name: 'Lorena', img: Image.asset('assets/avatar.jpeg')),
    IconName(name: 'Andres', img: Image.asset('assets/avatar.jpeg')),
    IconName(name: 'Patricia', img: Image.asset('assets/avatar.jpeg')),
    IconName(name: 'Pablo', img: Image.asset('assets/avatar.jpeg')),
    IconName(name: 'Lucia', img: Image.asset('assets/avatar.jpeg')),
    IconName(name: 'Daniel', img: Image.asset('assets/avatar.jpeg')),
    IconName(name: 'Daniela', img: Image.asset('assets/avatar.jpeg')),
    IconName(name: 'Cristian', img: Image.asset('assets/avatar.jpeg')),
    IconName(name: 'Yessica', img: Image.asset('assets/avatar.jpeg')),
    IconName(name: 'Maria', img: Image.asset('assets/avatar.jpeg')),
    IconName(name: 'Ana', img: Image.asset('assets/avatar.jpeg')),
  ];
}
