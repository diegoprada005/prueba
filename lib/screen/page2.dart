import 'package:flutter/material.dart';
import 'package:diseno_prueba/model/icon_name.dart';

import 'package:diseno_prueba/screen/search_delegate.dart';

import 'package:diseno_prueba/widgets/background.dart';

import 'package:provider/provider.dart';
import 'package:diseno_prueba/provider/navegation_icon.dart';

import '../widgets/botton_navigator_bar.dart';
import '../widgets/circle_avatar.dart';
import '../widgets/infinite_scroll.dart';

class Page2Screen extends StatelessWidget {
  const Page2Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
          children: [
            const Background(color: Color.fromARGB(255, 250, 249, 249)),
            SafeArea(
                child: Column(
              children: const [
                _Header(),
                SizedBox(height: 10),
                Expanded(child: Infinite(mssg: 'Best Photos'))
              ],
            )),
          ],
        ),
        bottomNavigationBar: ChangeNotifierProvider(
            create: (context) => NavegacionIcon(),
            child: const BottomNavigationBars()));
  }
}

class _Header extends StatelessWidget {
  const _Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      GestureDetector(
        child: Container(
          color: Colors.white,
          margin: const EdgeInsets.symmetric(horizontal: 20),
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            children: [
              CircleAvatar(
                child: Image.asset('assets/avatar.jpeg'),
              ),
              const SizedBox(width: 10),
              const Text('Hello, Cris',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              const Icon(Icons.handshake, color: Colors.yellow),
              Expanded(child: Container()),
              const Icon(Icons.align_horizontal_right_rounded)
            ],
          ),
        ),
        onTap: () => Navigator.pushNamed(context, 'page_3'),
      ),
      const _Search(),
      const _Peoples()
    ]);
  }
}

class _Search extends StatelessWidget {
  const _Search({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () =>
          showSearch(context: context, delegate: SearchDelegateResults()),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadiusDirectional.circular(8)),
        margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 35),
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: Row(
          children: const [
            Icon(Icons.search),
            SizedBox(width: 3),
            Text('Search photos or  photographes')
          ],
        ),
      ),
    );
  }
}

class _Peoples extends StatelessWidget {
  const _Peoples({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cant = IconName.data;
    return Column(children: [
      Container(
        margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: const Text('Popular Photographers',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      ),
      Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height * 0.1,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            physics: const BouncingScrollPhysics(),
            itemCount: IconName.data.length,
            itemBuilder: (BuildContext context, int index) {
              final nameP = cant[index].name;
              final imgP = cant[index].img;
              return Avatar(name: nameP, img: imgP, radius: 20);
            }),
      )
    ]);
  }
}
