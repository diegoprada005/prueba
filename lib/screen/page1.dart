import 'package:flutter/material.dart';

class Page1Screen extends StatelessWidget {
  const Page1Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: const [
          BackgroundImage(),
          _Information(),
        ],
      ),
    );
  }
}

class _Information extends StatelessWidget {
  const _Information({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      right: 80,
      bottom: 30,
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(width: 1, color: Colors.white),
            borderRadius: BorderRadius.circular(20)),
        height: 280,
        width: 290,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const _Text(
                text: 'Photography experience minimalist dimensions',
                type: FontWeight.bold,
                size: 20),
            const SizedBox(height: 10),
            const _Text(
                text:
                    'The ultimate dimensional photography experience with a minimalistic layout that creates a especial masterpiece',
                type: FontWeight.normal,
                size: 15),
            const SizedBox(height: 30),
            FloatingActionButton(
                backgroundColor: Colors.black,
                onPressed: () {
                  Navigator.pushNamed(context, 'page_2');
                },
                child: const Icon(Icons.arrow_forward_outlined))
          ],
        ),
      ),
    );
  }
}

class _Text extends StatelessWidget {
  final String text;
  final FontWeight type;
  final double size;

  const _Text(
      {Key? key, required this.text, required this.type, required this.size})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: size, fontWeight: type));
  }
}

class BackgroundImage extends StatelessWidget {
  const BackgroundImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * 0.9,
        alignment: Alignment.topCenter,
        child: const Image(image: AssetImage('assets/gatob.jpeg')));
  }
}
