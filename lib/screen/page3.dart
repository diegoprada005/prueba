import 'package:diseno_prueba/provider/navegation_icon.dart';
import 'package:diseno_prueba/widgets/circle_avatar.dart';
import 'package:diseno_prueba/widgets/infinite_scroll.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../widgets/botton_navigator_bar.dart';

class Page3Screen extends StatelessWidget {
  const Page3Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Stack(children: const [
            _Header(),
            _IconLeft(),
            _IconRight(),
            _CircleAvatar()
          ]),
          const Expanded(
              child: Infinite(
            mssg: 'Feature Photos',
          )),
          ChangeNotifierProvider(
              create: (context) => NavegacionIcon(),
              child: const BottomNavigationBars())
        ],
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
            height: MediaQuery.of(context).size.height * 0.3,
            child: const Image(image: AssetImage('assets/descarga.jpeg'))),
        const SizedBox(height: 100),
        const _RowButton()
      ],
    );
  }
}

class _IconRight extends StatelessWidget {
  const _IconRight({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Positioned(
      right: 30,
      top: 80,
      child: Icon(Icons.heart_broken, color: Colors.white),
    );
  }
}

class _IconLeft extends StatelessWidget {
  const _IconLeft({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
        left: 30,
        top: 80,
        child: Container(
            decoration: BoxDecoration(
                border: Border.all(width: 1, color: Colors.white),
                color: const Color.fromARGB(255, 212, 190, 190),
                borderRadius: BorderRadius.circular(20)),
            child: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(Icons.arrow_back))));
  }
}

class _CircleAvatar extends StatelessWidget {
  const _CircleAvatar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 230,
      left: 150,
      child: Avatar(
        img: Image.asset('assets/avatar.jpeg'),
        name: 'Hello, Cris',
        radius: 50,
      ),
    );
  }
}

class _RowButton extends StatelessWidget {
  const _RowButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        _FloatingActionButton(
            text: 'Follow', color: Colors.black, colortext: Colors.white),
        SizedBox(width: 30),
        _FloatingActionButton(
            text: 'Message', color: Colors.white, colortext: Colors.black),
      ],
    );
  }
}

class _FloatingActionButton extends StatelessWidget {
  final Color colortext;
  final String text;
  final Color color;
  const _FloatingActionButton({
    Key? key,
    required this.text,
    required this.color,
    required this.colortext,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(primary: color),
        onPressed: () {},
        child: Text(
          text,
          style: TextStyle(color: colortext),
        ));
  }
}
