import 'package:diseno_prueba/screen/page1.dart';
import 'package:diseno_prueba/screen/page2.dart';
import 'package:diseno_prueba/screen/page3.dart';

import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ToastContext().init(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      initialRoute: 'page_3',
      routes: {
        'page_1': (_) => const Page1Screen(),
        'page_2': (_) => const Page2Screen(),
        'page_3': (_) => const Page3Screen(),
      },
    );
  }
}
