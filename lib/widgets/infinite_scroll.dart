import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class Infinite extends StatefulWidget {
  final String mssg;
  const Infinite({
    Key? key,
    required this.mssg,
  }) : super(key: key);

  @override
  State<Infinite> createState() => _InfiniteState();
}

class _InfiniteState extends State<Infinite> {
  final List<int> imagesId = [1, 2, 3, 4, 5, 6, 7, 8, 9];

  final ScrollController scrollController = ScrollController();
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    scrollController.addListener(() {
      fetchDay();
    });
  }

  void add() {
    final lastId = imagesId.last;
    imagesId.addAll([1, 2, 3, 4, 5].map((e) => lastId + e));
    setState(() {
      if ((scrollController.position.pixels + 500) >=
          scrollController.position.maxScrollExtent) {
        add();
      }
    });
  }

  Future fetchDay() async {
    if (isLoading) return;
    isLoading = true;
    setState(() {});

    Future.delayed(const Duration(seconds: 3));
    add();
    isLoading = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.mssg,
          style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.4,
          child: MasonryGridView.count(
              crossAxisSpacing: 1,
              mainAxisSpacing: 1,
              crossAxisCount: 2,
              itemBuilder: (context, index) {
                return Container(
                  margin: const EdgeInsets.all(8),
                  height: (index % 5 + 1) * 100,
                  child: _Image(index: index),
                );
              }),
        ),
      ],
    );
  }
}

class _Image extends StatelessWidget {
  final int index;
  const _Image({
    Key? key,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FadeInImage(
        fit: BoxFit.cover,
        placeholder: const AssetImage('assets/camera.jpeg'),
        image: NetworkImage(fullPosterImg(index)));
  }
}

fullPosterImg(int index) {
  if (index != null) return 'https://picsum.photos/200/300?image=${index + 1}';

  return 'https://i.stack.imgur.com/GNhxO.png';
}
