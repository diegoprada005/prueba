import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  final Color color;
  const Background({Key? key, required this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
    );
  }
}
