import 'package:diseno_prueba/provider/navegation_icon.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

class BottomNavigationBars extends StatelessWidget {
  const BottomNavigationBars({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final currentIndex = Provider.of<NavegacionIcon>(context);
    return BottomNavigationBar(
      currentIndex: currentIndex.paginaActual,
      backgroundColor: Colors.red,
      unselectedItemColor: Colors.deepOrange,
      selectedItemColor: Colors.blue,
      onTap: (i) {
        currentIndex.paginaActual = i;

        toastMsg(context, i);
      },
      items: const [
        BottomNavigationBarItem(
            icon: Icon(Icons.home, color: Colors.black), label: ''),
        BottomNavigationBarItem(
            icon: Icon(Icons.image_sharp, color: Colors.black), label: ''),
        BottomNavigationBarItem(
            icon: Icon(Icons.camera, color: Colors.black), label: ''),
        BottomNavigationBarItem(
            icon: Icon(Icons.add_box_outlined, color: Colors.black), label: ''),
        BottomNavigationBarItem(
            icon: Icon(Icons.person, color: Colors.black), label: ''),
      ],
    );
  }
}

void toastMsg(BuildContext context, int valor) {
  if (valor == 0) {
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Presiono el boton de la casa'),
        backgroundColor: Colors.red));
  } else if (valor == 1) {
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Presiono el boton de la Galeria'),
        backgroundColor: Colors.red));
  } else if (valor == 2) {
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Presiono el boton de la camara'),
        backgroundColor: Colors.red));
  } else if (valor == 3) {
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Presiono el boton del mas '),
        backgroundColor: Colors.red));
  } else if (valor == 4) {
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Presiono el boton de la persona '),
        backgroundColor: Colors.red));
  }
}
