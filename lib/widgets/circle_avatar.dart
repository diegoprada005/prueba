import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {
  final double radius;
  final Image img;
  final String name;
  const Avatar({
    Key? key,
    required this.img,
    required this.name,
    required this.radius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: [CircleAvatar(radius: radius, child: img), Text(name)],
      ),
    );
  }
}
